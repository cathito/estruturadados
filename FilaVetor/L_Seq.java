package FilaVetor;

import java.util.Arrays;

public class L_Seq {
	int [] ListaSequencial;
	
//============================================================================//
	public L_Seq() {
		this.ListaSequencial=new int [10];
	}
	public L_Seq(int[] vetor) {
		this.ListaSequencial=vetor;
	}
//============================================================================//
	
	public void addInicio(int valor) {
		if(ultimoIndiceLivre(ListaSequencial) ==ListaSequencial.length) 
			System.out.println("Vetor Cheio");
		else
			ListaSequencial[ultimoIndiceLivre(ListaSequencial)]=valor;
		
	}
//========================================================================//
	
	public void Remover() {
		for (int i = 0; i < ListaSequencial.length; i++) {
			if(i<ListaSequencial.length-1) {
				ListaSequencial[i]=ListaSequencial[i+1];
			}else {
				ListaSequencial[ListaSequencial.length-1]=0;
			}
		}

	}
//--------------------------------------------------------------------------//
	
	public void removerTudo() {
		this.ListaSequencial=new int [10];
	}
	
//============================================================================//
	
	public int ultimoIndiceLivre(int[] vetor) {
		for (int i = 0; i < vetor.length; i++) {
			if(vetor[i]==0) {
				return i;
			}
		}
		return vetor.length;
	}
//--------------------------------------------------------------------------//
	
	public void Listar() {
		for (int i = 0; i < ListaSequencial.length; i++) {
			System.out.print(ListaSequencial[i]+", ");
		}
	}
//============================================================================//
	
	public boolean compararArray(Object o) {
		int []vetor= (int[])o;
		
	    try {
	    	if(vetor.length!=ListaSequencial.length)
	    		return false;
	    	
		    for (int i = 0; i < vetor.length; i++) {
			    if (vetor[i] != ListaSequencial[i]) {
				    return false;
			    }
		    }
	    } catch (IndexOutOfBoundsException e) {//esta excep��o aparece caso os arrays tenham tamanhos diferentes
		    return false;
	    }
	    return true; //caso n�o tenha devolvido falso � porque s�o iguais
	    
	}
//==================================================================================//
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		
		L_Seq other = (L_Seq) obj;
		
		if (!Arrays.equals(ListaSequencial, other.ListaSequencial))
			return false;
		
		return true;
	}
	
	
	
	
	
	
	

}
