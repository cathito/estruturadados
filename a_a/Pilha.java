package a_a;

public class Pilha {
	
	private No inicio;
	private No topo;
	int qtdListas=0;
//----------------------------------------------------//
	
	public void empilhar(char valor) {
		No novo = new No(valor);

		if (inicio == null){
			this.inicio = novo;
			topo=inicio;
		}else {
			novo.proximo=inicio;
			inicio=novo;
			topo=novo;
		}
		qtdListas++;
	}
//----------------------------------------------------//
	public void desempilhar() {
		if(qtdListas==0) {
			System.out.println("A Pilha esta Vazia!");
			return;
		}
		No Atual= inicio;
		if(Atual!=null){
			inicio=Atual.proximo;
			qtdListas--;
		}

	}
//====================================================================//
	
	public void pilhaDiversidade(Object obj) {
		Pilha copParametro = (Pilha) obj;
		Pilha copOrig=this;
		LSE pd= new LSE();
		
	//---------------- Verificar toda pilha 1 ----------------//
		
		for (int i = 0; i < copOrig.qtdListas; i++) {
			boolean naotem=true;
			
			for (int j = 0; j < copParametro.qtdListas; j++) {
				if(copParametro.topo.dado==copOrig.topo.dado) {
					naotem=false;
					break;
				}
				copParametro.desempilhar();
			}
			if(naotem) {
				pd.AdicionarInicio(copOrig.topo.dado);
			}
			copOrig.desempilhar();
			copParametro = (Pilha) obj;
		}
		
		
	//---------------- Verificar toda pilha 2 ----------------//
		copParametro = (Pilha) obj;
		copOrig=this;
		for (int i = 0; i < copParametro.qtdListas; i++) {
			boolean naotem=true;
			
			for (int j = 0; j < copOrig.qtdListas; j++) {
				if(copOrig.topo.dado==copParametro.topo.dado) {
					naotem=false;
					break;
				}
				copOrig.desempilhar();
			}
			if(naotem) {
				pd.AdicionarInicio(copOrig.topo.dado);
			}
			copParametro.desempilhar();
			copOrig=this;
		}
	//-------------------------------------------------------//
		pd.Listar();
		
	}
	
	
	
	
//====================================================================//
	public void Listar() {
		No novo= inicio;
		while(novo!=null) {
			System.out.println("| "+novo.dado+" |");
			novo=novo.proximo;
		}
		System.out.println("Tamanho da Pilha � : "+qtdListas);
	}
//----------------------------------------------------//
	
	
	
	
	
	
	
	
	

}
