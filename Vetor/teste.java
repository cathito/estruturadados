package Vetor;

import java.util.Random;

public class teste {

	public static void main(String[] args) {
		Random random = new Random();
	//--------------------------------------------------// 
		
		int [] vetor=new int [10];
		vetor[0]=3;
		vetor[1]=4;
		vetor[2]=10;
		vetor[3]=11;
	//--------------------------------------------------//
		
			//10 n�meros aleat�rios entre 0 e 25
		int [] vetor2=new int [10];
        for (int i = 0; i < 10; i++) {
        	vetor2[i]=random.nextInt(26);
        }
    //--------------------------------------------------//
        
		L_Seq mover= new L_Seq(vetor);
		
//================================================================================================//
		
		
		
		System.out.print("\n  Vetor Original: ");
		mover.print();
		System.out.println("\n=====================================================");
		
		
		System.out.println("\n\n         + + +  ADICAO DE VALORES  + + +");
		System.out.println("    -----------------------------------------\n\n");
		
		System.out.println("  Adicionar Valor Na Primeira Posicao");
		mover.addInicio(2);
		mover.addInicio(1);
		mover.print();
		System.out.println("\n=====================================================");
		
//================================================================================================//
		
		System.out.println("\n  Situacao Atual do Vetor: ");
		mover.print();
		
		System.out.println("\n\n  Adicionar Valor Em Quaisquer Posicao\n");
		
		System.out.println(" (A) Posicao Inesistente");
		mover.addMeio(10, 10);
		
		System.out.println("\n (B)Posicao Posicao Preenchida");
		mover.addMeio(3, 5);
		mover.print();
		System.out.println();
		mover.addMeio(4, 6);
		mover.print();
		
		System.out.println("\n=====================================================");
		
//================================================================================================//
		System.out.println("\n  Situacao Atual do Vetor: ");
		mover.print();
		
		System.out.println("\n\n  Adicionar Valor Na Ultima Posicao");
		mover.addUltimaPosicaoLivre(25);
		mover.addUltimaPosicaoLivre(30);
		mover.print();
		System.out.println("\n");
		mover.addUltimaPosicaoLivre(35);
		mover.addUltimaPosicaoLivre(40);
		mover.print();
		System.out.println("\n=====================================================");
		
		
//=======================================================================================================//
//=======================================================================================================//
		System.out.println("\n  Situacao Atual do Vetor: ");
		mover.print();
		
		System.out.println("\n\n         + + +  REMOCAO DE VALORES  + + +");
		System.out.println("    -----------------------------------------\n\n");
		
		System.out.println("  Remover Valor Na Primeira Posicao");
		mover.removerInicio();
		mover.print();
		System.out.println("\n");
		
		mover.removerInicio();
		mover.print();
		System.out.println("\n=====================================================\n\n");
		
		System.out.println("  Remover Valor Em Quaisquer Posicao");
		mover.removerMeio(3);
		mover.print();
		System.out.println("\n");
		
		mover.removerMeio(1);
		mover.print();
		System.out.println("\n=====================================================\n\n");
		
		System.out.println("  Remover Valor Na Ultima Posicao Livre");
		mover.removerUltimoValor();
		mover.print();
		System.out.println("\n=====================================================\n\n");
		
		System.out.println("  Remover Tudo!");
		mover.removerTudo();
		mover.print();
		System.out.println("\n=====================================================\n\n");
//=======================================================================================================//
		
		
		
		
	}

}
