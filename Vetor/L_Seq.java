package Vetor;

import java.util.Arrays;

public class L_Seq {
	int [] ListaSequencial;
	
//============================================================================//
	public L_Seq() {
		this.ListaSequencial=new int [10];
	}
	public L_Seq(int[] vetor) {
		this.ListaSequencial=vetor;
	}
//============================================================================//
	
	public void addInicio(int valor) {
		if(ListaSequencial[0]==0) {
			ListaSequencial[0]=valor;
		}else {
			for (int i= ultimoIndiceLivre(ListaSequencial); i>= 0; i--) {
				if(i>0) 
					ListaSequencial[i]=ListaSequencial[i-1];
				else 
					ListaSequencial[i]=valor;
			}
		}
	}
//--------------------------------------------------------------------------//
	
	public void addMeio(int posicao,int valor) {
		
		if(ListaSequencial.length<=posicao || posicao<=0)
			System.out.println("     Posicao Invalida!");
		
		else {
			if( (ultimoIndiceLivre(ListaSequencial)+1) == posicao) 
				ListaSequencial[posicao]=valor;
			else {
				for (int i= ultimoIndiceLivre(ListaSequencial); i>= posicao; i--) {
					if(i>posicao) 
						ListaSequencial[i]=ListaSequencial[i-1];
					else 
						ListaSequencial[i]=valor;
				}
			}
		}
		
	}
//--------------------------------------------------------------------------//
	
	public void addUltimaPosicaoLivre(int valor) {
		if(ultimoIndiceLivre(ListaSequencial) ==ListaSequencial.length) 
			System.out.println("Vetor Cheio");
		else
			ListaSequencial[ultimoIndiceLivre(ListaSequencial)]=valor;
		
	}
//========================================================================//
	
	public void removerInicio() {
		for (int i = 0; i < ListaSequencial.length; i++) {
			if(i<ListaSequencial.length-1) {
				ListaSequencial[i]=ListaSequencial[i+1];
			}else {
				ListaSequencial[ListaSequencial.length-1]=0;
			}
		}

	}
//--------------------------------------------------------------------------//
	
	public void removerMeio(int posicao) {
		if(ListaSequencial.length<=posicao || posicao<=0)
			System.out.println("     Posicao Invalida!");
		else {
			for (int i= posicao-1; i< ListaSequencial.length-1; i++) {
				ListaSequencial[i]=ListaSequencial[i+1];
			}
		}

	}
//--------------------------------------------------------------------------//
	
	public void removerUltimoValor() {
		//System.out.println(ListaSequencial[ultimoIndiceLivre(ListaSequencial)-1]);
		ListaSequencial[ultimoIndiceLivre(ListaSequencial)-1]=0;
		
	}
//--------------------------------------------------------------------------//
	
	public void removerTudo() {
		this.ListaSequencial=new int [10];
	}
	
//============================================================================//
	
	public int ultimoIndiceLivre(int[] vetor) {
		for (int i = 0; i < vetor.length; i++) {
			if(vetor[i]==0) {
				return i;
			}
		}
		return vetor.length;
	}
//--------------------------------------------------------------------------//
	
	public void print() {
		for (int i = 0; i < ListaSequencial.length; i++) {
			System.out.print(ListaSequencial[i]+", ");
		}
	}
//============================================================================//
	
	public boolean compararArray(Object o) {
		int []vetor= (int[])o;
		
	    try {
	    	if(vetor.length!=ListaSequencial.length)
	    		return false;
	    	
		    for (int i = 0; i < vetor.length; i++) {
			    if (vetor[i] != ListaSequencial[i]) {
				    return false;
			    }
		    }
	    } catch (IndexOutOfBoundsException e) {//esta excep��o aparece caso os arrays tenham tamanhos diferentes
		    return false;
	    }
	    return true; //caso n�o tenha devolvido falso � porque s�o iguais
	    
	}
//==================================================================================//
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		
		L_Seq other = (L_Seq) obj;
		
		if (!Arrays.equals(ListaSequencial, other.ListaSequencial))
			return false;
		
		return true;
	}
	
	
	
	
	
	
	

}
