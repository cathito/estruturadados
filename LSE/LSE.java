package LSE;

public class LSE {
	
	private No inicio;
	private No fim;
	int qtdListas=0;
//----------------------------------------------------//
	
	public void AdicionarInicio(int valor) {
		No novo = new No(valor);
		
		if(inicio==null ) {
			this.inicio=novo;
			this.fim=novo;
		}else {
			novo.proximo=inicio;
			inicio=novo;
		}
		qtdListas++;
	}
//----------------------------------------------------//
	public void AdicionarMeio(int posicao, int valor) {
		No novo= new No(valor);
		No atual= inicio;
		
		if(posicao<2) 
    		AdicionarInicio(valor);
    	
    	if(posicao>this.qtdListas) 
    		AdicionarFim(valor);
    	
    	else {
			for (int i = 1; i < posicao-1; i++) {
				atual=atual.proximo;
			}
			novo.proximo=atual.proximo;
			atual.proximo=novo;
			qtdListas++;
    	}
	}
//----------------------------------------------------//
	
	public void AdicionarFim(int valor) {
		No novo = new No(valor);
		fim.proximo=novo;
		fim=novo;
		qtdListas++;
		
	}
//=============================================================================//
	
	public void RemoverInicio() {
		inicio=inicio.proximo;
		qtdListas--;
		
	}
//----------------------------------------------------//
	public void RemoverMeio(int posicao) {
		No atual=inicio;
		
		if(posicao<2) 
    		RemoverInicio();
    	
    	if(posicao>this.qtdListas) 
    		RemoverFim();
    	else {
    		for (int i = 1; i < posicao-1; i++) {
    			atual=atual.proximo;
    		}
    		
    		atual.proximo=atual.proximo.proximo;
    		qtdListas--;
    	}
    	
	}
//----------------------------------------------------//
	public void RemoverFim() {
		No atual= inicio;
		for (int i = 1; i < qtdListas-1; i++) {
			atual=atual.proximo;
		}
		atual.proximo=null;
		fim=atual;
		qtdListas--;
		
	}
//=============================================================================//
	public void encontrarValor() {
		

	}
	
	
	
	
//=============================================================================//
	
	public void Listar() {
		No novo= inicio;
		while(novo!=null) {
			System.out.print(novo.dado+", ");
			novo=novo.proximo;
		}
		System.out.println("\nTamanho da lista � : "+qtdListas);
	}
//=============================================================================//
	
	
	
	
	
	
	

}
