package LSE;

public class Pessoa implements Comparable {
	public String nome;
	public int idade;
	
	@Override
	public int compareTo(Object o) {
		Pessoa p = (Pessoa) o;
		if(this.idade > p.idade ) {
			return 1;
		}else if(this.idade < p.idade) {
			return -1;
		}
		return 0;
	}
	
	public int compareNome(Pessoa o) {
		return this.nome.compareTo(o.nome);
	}
	
}
