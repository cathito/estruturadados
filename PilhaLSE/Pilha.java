package PilhaLSE;

public class Pilha {
	
	private No inicio;
	private No topo;
	int qtdListas=0;
//----------------------------------------------------//
	
	public void empilhar(int valor) {
		System.out.println("Empilhando o valor "+valor);
		No novo = new No(valor);

		if (inicio == null){
			this.inicio = novo;
			topo=inicio;
		}else {
			novo.proximo=inicio;
			inicio=novo;
			topo=novo;
		}
		qtdListas++;
	}
//----------------------------------------------------//
	public void desempilhar() {
		System.out.println("\nDesempilhando...");
		No Atual= inicio;
		if(Atual!=null){
			inicio=Atual.proximo;
			qtdListas--;
		}

	}
//====================================================================//
	public void encontrarValor(int valor) {
		Pilha lsAuxiliar=this;
		
		No novo= lsAuxiliar.inicio;
		int elementoNumero=0;
		
		while(novo!=null) {
			elementoNumero++;
			
			if(novo.dado==valor) {
				System.out.println(
						    "               Valor Encontrado! "
						+ "\nValor correspodente ao "+elementoNumero+"� elemento a ser Desempilhado!");
				break;
			}
			desempilhar();
			novo=novo.proximo;
		}
		

	}
//---------------------------------------------------------------------------//
	public void igual(Object obj) {
		
	}
	
	
//====================================================================//
	public void Listar() {
		No novo= inicio;
		while(novo!=null) {
			System.out.println("| "+novo.dado+" |");
			novo=novo.proximo;
		}
		System.out.println("Tamanho da Pilha � : "+qtdListas);
	}
//----------------------------------------------------//
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass() )
			return false;
		
		Pilha other = (Pilha) obj;
		if (inicio == null) {
			if (other.inicio != null)
				return false;
		} else if (!inicio.equals(other.inicio))
			return false;
		if (qtdListas != other.qtdListas)
			return false;
		if (topo == null) {
			if (other.topo != null)
				return false;
		} else if (!topo.equals(other.topo))
			return false;
		return true;
	}
	
	
	
	
	
	
	

}
