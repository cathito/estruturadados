package a;


public class Pilha {
	
	private No inicio;
	private No topo;
	int qtdListas=0;
//----------------------------------------------------//
	
	public void empilhar(int valor) {
		System.out.println("Empilhando o valor "+valor);
		No novo = new No(valor);

		if (inicio == null){
			this.inicio = novo;
			topo=inicio;
		}else {
			novo.proximo=inicio;
			inicio=novo;
			topo=novo;
		}
		qtdListas++;
	}
//----------------------------------------------------//
	public void desempilhar() {
		if(qtdListas==0) {
			System.out.println("A Pilha esta Vazia!");
			return;
		}
		System.out.println("\nDesempilhando...");
		No Atual= inicio;
		if(Atual!=null){
			inicio=Atual.proximo;
			qtdListas--;
		}

	}
//====================================================================//
	public boolean igual(Object obj) {
		Pilha copParametro = (Pilha) obj;
		Pilha copOrig=this;
		System.out.println("Tamanho P Original "+copOrig.qtdListas+" Pilha Parametro "+copParametro.qtdListas);
		
		if(copOrig.qtdListas!=copParametro.qtdListas) {
			return false;
		}
		for (int i = 0; i <qtdListas; i++) {
			if(copOrig.topo.dado !=copParametro.topo.dado ) {
				return false;
			}
			copOrig.desempilhar();
			copParametro.desempilhar();
			
		}
		return true;
		
	}
	
	
//---------------------------------------------------------------//
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass() )
			return false;
		
		Pilha other = (Pilha) obj;
		if (inicio == null) {
			if (other.inicio != null)
				return false;
		} else if (!inicio.equals(other.inicio))
			return false;
		if (qtdListas != other.qtdListas)
			return false;
		if (topo == null) {
			if (other.topo != null)
				return false;
		} else if (!topo.equals(other.topo))
			return false;
		return true;
	}
//====================================================================//
	public void Listar() {
		No novo= inicio;
		while(novo!=null) {
			System.out.println("| "+novo.dado+" |");
			novo=novo.proximo;
		}
		System.out.println("Tamanho da Pilha � : "+qtdListas);
	}
//----------------------------------------------------//
	
	
	
	
	
	
	
	
	

}
