package FilaSEC;

public class TesteLista {//empilhar, desempilhar,topo

	public static void main(String[] args) {
		FilaSEC ls= new FilaSEC();
		
		System.out.println("\nAdicionando Valores Na Fila.\n");
		ls.Adicionar(1);
		ls.Adicionar(2);
		ls.Adicionar(3);
		ls.Adicionar(4);
		ls.Adicionar(5);
		System.out.println("");
		ls.Listar();
		System.out.println("\n\n===============================\n");
//-------------------------------------------------------------------------------//

		System.out.println("Removendo Valores da Fila.");
		ls.Remover();
		ls.Listar();
		System.out.println("\n--------------------------");
		ls.Remover();
		ls.Listar();
		System.out.println("\n\n===============================\n");
		
//-------------------------------------------------------------------------------//
		System.out.println("\nAdicionando Valor Na Fila.\n");
		ls.Adicionar(6);
		ls.Listar();

//-------------------------------------------------------------------------------//



	}

}
