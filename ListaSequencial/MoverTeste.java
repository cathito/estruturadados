package ListaSequencial;

public class MoverTeste {

	public static void main(String[] args) {
		int [] vetor= {1,2,5,6,7,9,10,0,0,0,0,0,0};
		
		System.out.println("\n              Trabalhando Com Vetores!");
		
		MoverValores mover= new MoverValores();
		System.out.print("\n  Vetor Original: ");
		mover.print(vetor);
		System.out.println("\n===================================================");
//-----------------------------------------------------------------------------------//
		
		System.out.println("\n  Adicionar Valor Na Primeira Posicao");
		mover.PrimeiraPosicao(vetor, -1);
		mover.PrimeiraPosicao(vetor, -2);
		mover.print(vetor);
		System.out.println("\n------------------------------------------");
//-----------------------------------------------------------------------------------//	
		
		System.out.println("\n  Situacao Atual do Vetor: ");
		mover.print(vetor);
		
		System.out.println("\n  Adicionar Valor Em Quaisquer Posicao:");
		
		System.out.println("\n\n (A) Posicao Inesistente");
		mover.Meio(vetor, 100, 10);
		mover.print(vetor);
		
		System.out.println("\n\n (B)Posicao Posicao Preenchida");
		mover.Meio(vetor, 3, 5);
		mover.print(vetor);
		
		System.out.println();
		mover.Meio(vetor, 4, 6);
		mover.print(vetor);
		
		System.out.println("\n------------------------------------------");
//-----------------------------------------------------------------------------------//
		
		System.out.println("\n  Adicionar Valor Na Ultima Posicao");
		System.out.println("\n Situacao Atual do Vetor: ");
		mover.print(vetor);
		
		System.out.println("\n\n Ultima Posicao Livre ");
		mover.UltimaPosicao(vetor, 999);
		mover.print(vetor);
		
		System.out.println("\n\n  Ultima Posicao Existente\n Em Andamento!");
		mover.print(vetor);
		
		
//-----------------------------------------------------------------------------------//
		
		System.out.println("\n============================================\n");
		
		System.out.println("  Remover Determinado valor");
		
		System.out.println("\n Situacao Atual do Vetor: ");
		mover.print(vetor);
		
		System.out.println("\n\n  Remover Valor da Primeira Posicao");
		mover.removerPrimeirao(vetor);
		mover.print(vetor);
		System.out.println("\n------------------------------------------");
	//-------------------------------------------------------------------------//
		System.out.println("\n Situacao Atual do Vetor: ");
		mover.print(vetor);
		
		System.out.println("\n\n  Remover Valor Em Quaisquer Posicao");
		mover.removerMeio(vetor, 2);
		mover.print(vetor);
		
		System.out.println("\n------------------------------------------");
		
		
		
		
		
		
		
	}

}
