package ListaSequencial;

public class MoverValores {
	
	public void PrimeiraPosicao(int[] vetor,int valor) {
		if(vetor[0]==0) {
			vetor[0]=valor;
		}else {
			for (int i= ultimoIndiceLivre(vetor); i>= 0; i--) {
				if(i>0) 
					vetor[i]=vetor[i-1];
				else 
					vetor[i]=valor;
			}
		}
	}
//--------------------------------------------------//
	public void Meio(int[] vetor,int posicao,int valor) {
		
		if(vetor.length<=posicao || posicao<0)
			System.out.println("   Resposta: Posicao Invalida!");
		
		else if(vetor[posicao]==0 ) { 
			vetor[posicao]=valor;
		}
		
		else {//posicao ocupada, � preciso empurrar os demais.
			
			for (int i= ultimoIndiceLivre(vetor); i>= posicao; i--) {
				if(i>posicao) 
					vetor[i]=vetor[i-1];
				else 
					vetor[i]=valor;
			}
		}

	}
	
//--------------------------------------------------//
	public void UltimaPosicao(int[] vetor,int valor) {
		for (int i = vetor.length-1; i>=0; i--) {
			
			if(vetor[i]==0 && vetor[i-1]!=0) // estou supondo q o vetor esta sendo realocado a cada remocao no meio
				vetor[i]=valor;
			
		}
	}
	
//==============================================================//
	
	public void removerPrimeirao(int[] vetor) {
		
		for (int i = 0; i < vetor.length; i++) {
			if(i<vetor.length-1) {
				vetor[i]=vetor[i+1];
			}else {
				vetor[vetor.length-1]=0;
			}
		}

	}
//---------------------------------------------------------------//
	
	public void removerMeio(int[] vetor,int posicao) {
		if(vetor.length<=posicao)
			System.out.println("   Resposta: Posicao Invalida!");
		else {
			for (int i= posicao; i< vetor.length-1; i++) {
				vetor[i]=vetor[i+1];
			}
		}

	}
//---------------------------------------------------------------//
	
	public void removerUltimo(int[] vetor) {
		vetor[vetor.length]=0;
		
	}
//==============================================================//
	
	public int ultimoIndiceLivre(int[] vetor) {
		for (int i = 0; i < vetor.length; i++) {
			if(vetor[i]==0) {
				return i;
			}
		}
		return -1;
	}
//==============================================================//
	
	public void print(int[] v) {
		for (int i = 0; i < v.length; i++) {
			System.out.print(v[i]+", ");
		}
	}
//-----------------------------------------------------//
	
	

}
