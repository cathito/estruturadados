package LDE_Circular;


public class LDEC {
    private No inicio;
    int qtdListas=0;

    public void AdicionarInicio(int valor) {
        No novo = new No(valor);
        
        System.out.println("Adicionando o Valor: "+valor);
        
        if(inicio==null ) {
            novo.anterior=novo;
            novo.proximo=novo;
        }else {
            novo.anterior=inicio.anterior;
            inicio.anterior.proximo=novo;
            inicio.anterior=novo;
            novo.proximo=inicio;
            
        }
        inicio=novo;
        qtdListas++;

    }
//--------------------------------------------------------------------------//
    public void AdicionarMeio(int posicao, int valor ) {
    	
    	System.out.println("Adicionando o Valor: "+valor+" na Posicao "+posicao);
    	
    	if(posicao<2) 
    		AdicionarInicio(valor);
    	
    	else if(posicao>this.qtdListas) 
    		AdicionarFim(valor);
    	
    	else {
	        No novo= new No(valor);
	        No atual=inicio;
	        
	        for (int i = 1; i < posicao-1; i++) {
	            atual=atual.proximo;
	        }
	        novo.proximo=atual.proximo;
	    	novo.anterior=atual;
	    	atual.proximo=novo;
	    	novo.proximo.anterior=novo;
	    	qtdListas++;
    	}
    	
    }
//--------------------------------------------------------------------------//

    public void AdicionarFim(int valor) {
    	System.out.println("Adicionando o Valor: "+valor);
    	
    	No novo = new No(valor);
        
        novo.anterior=inicio.anterior;
        novo.proximo=inicio;
        inicio.anterior.proximo=novo;
        inicio.anterior=novo;
        qtdListas++;

    }
//--------------------------------------------------------------//
    
    public void RemoverInicio() { 
    	No posteriorInicio=inicio.proximo;
    	inicio=posteriorInicio;
    	RemoverFim();
    }
  //--------------------------------------------------------//
    public void RemoverMeio(int posicao) {
    	System.out.println("Removendo o Valor da posicao: "+posicao);
    	if(posicao<2) 
    		RemoverInicio();
    	
    	if(posicao>this.qtdListas) 
    		RemoverFim();
    	
    	else{
    		No atual=inicio;
	    	for (int i = 1; i < posicao-1; i++) {
	            atual=atual.proximo;
	        }
	    	atual.proximo.proximo.anterior=atual;
	    	atual.proximo=atual.proximo.proximo;
	    	qtdListas--;
    	}
    	
    }
  //--------------------------------------------------------//
    public void RemoverFim() {
    	No atual=inicio;
    	
    	atual.anterior.anterior.proximo=inicio;
    	atual.anterior=atual.anterior.anterior;
    	inicio=atual;
    	
    	qtdListas--;
    }
//--------------------------------------------------------------------------//
    
    public void BuscarValor(int valor){
    	if(encontrarValor(valor)) 
    		System.out.println("O valor "+valor+" foi encontrado na posicao: "+Posicao(valor) );
    	else 
    		System.out.println("O valor "+valor+" n�o foi encontrado!");
    }
    
    private boolean encontrarValor(int valor){
        No atual=inicio;
        
        for (int i=0; i < qtdListas; i++) {
            if(atual.dado==valor)
                return true;
            atual=atual.proximo;
        }
		return false;
    }
    
    private int Posicao(int valor){
        No atual=inicio;
        int i;
        for (i=1; i <= qtdListas; i++) {
            if(atual.dado==valor)
                break;
            atual=atual.proximo;
        }
		return i;
    }
  //--------------------------------------------------------//
    
    public void trocaValorLocal(int valor,int posicao){
        if(encontrarValor(valor)){
        	BuscarValor(valor);
        	
            RemoverMeio(Posicao(valor));
            
            AdicionarMeio(posicao, valor);
        }
        
    }

//================================================================================//
    
    public void Listar() {
    	No novo= inicio;
    	int pulo=0;
        int contadorLup=0;

        System.out.println("\nPrintando Lista: ");
        
        while(true) {
        	
            if( pulo==qtdListas){
            	System.out.print(" //  ");
            	pulo=0;
            }
            if (contadorLup==3*qtdListas)
            	break;
            
            System.out.print(novo.dado+", ");
            novo=novo.proximo;
            contadorLup++;
            pulo++;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
    }
//--------------------------------------------------------------------------//

    public void ListarContrario() {
    	No novo= inicio.anterior;
    	int pulo=0;
        int contadorLup=0;

        System.out.println("\nPrintando Lista Ao Contrario: ");
        while(true) {
        	if( pulo==qtdListas){
            	System.out.print(" //  ");
            	pulo=0;
            }
            if (contadorLup==3*qtdListas)
            	break;
            
            System.out.print(novo.dado+", ");
            novo=novo.anterior;
            contadorLup++;
            pulo++;
        }
        System.out.println("\nTamanho da Lista: "+this.qtdListas);
    }
//--------------------------------------------------------------------------//*/


}
