package LDE_Circular;

public class Teste {
    public static void main(String[] args) {
        LDEC LDEC = new LDEC();

        System.out.println("            Adicionando Valores No Inicio!");
        LDEC.AdicionarInicio(9);
        LDEC.AdicionarInicio(8);
        LDEC.AdicionarInicio(7);
        LDEC.AdicionarInicio(6);
        LDEC.AdicionarInicio(3);
        LDEC.AdicionarInicio(2);
        LDEC.AdicionarInicio(1);
        LDEC.Listar();
        LDEC.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
//-----------------------------------------------------------------------------------------------//
        

        System.out.println("            Adicionando Valores No Meio!");
        LDEC.AdicionarMeio(4,4);
        LDEC.AdicionarMeio(5,5);
        LDEC.Listar();
        LDEC.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
//-----------------------------------------------------------------------------------------------//
        
        System.out.println("            Adicionando Valores No Fim!");
        LDEC.AdicionarFim(10);
        LDEC.AdicionarFim(11);
        LDEC.Listar();
        LDEC.ListarContrario();
//-----------------------------------------------------------------------------------------------//
        
        System.out.println("\n=============================================================================\n\n");
        
        System.out.println("            Buscando Valores!\n");
        LDEC.BuscarValor(3);
        LDEC.BuscarValor(8);
        LDEC.BuscarValor(0);
        System.out.println("\n=============================================================================\n\n");
        
//-----------------------------------------------------------------------------------------------//
        System.out.println("            Trocando Valor de Local!\n");
        LDEC.trocaValorLocal(3, 1);
        LDEC.Listar();
        System.out.println("\n\n");
        
        LDEC.trocaValorLocal(3, 3);
        LDEC.Listar();
        LDEC.ListarContrario();
        System.out.println("\n=============================================================================\n\n");
        
        
//-----------------------------------------------------------------------------------------------//
        
        
        System.out.println("            Remover Valores No Inicio!");
        LDEC.RemoverInicio();
        LDEC.Listar();
        LDEC.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
        
        System.out.println("            Remover Valores No Meio!");
        LDEC.RemoverMeio(3);
        LDEC.Listar();
        LDEC.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
        
        System.out.println("            Remover Valores No Fim!");
        LDEC.RemoverFim();
        LDEC.Listar();
        LDEC.ListarContrario();
        System.out.println("\n=============================================================================\n\n");
//-----------------------------------------------------------------------------------------------//
        
        
        
        
        
    }


}
