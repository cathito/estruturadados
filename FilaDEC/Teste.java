package FilaDEC;

public class Teste {
    public static void main(String[] args) {
        LDEC LDEC = new LDEC();

        System.out.println("            Adicionando Valores No Inicio!");
        LDEC.AdicionarInicio(9);
        LDEC.AdicionarInicio(8);
        LDEC.AdicionarInicio(7);
        LDEC.AdicionarInicio(6);
        LDEC.AdicionarInicio(3);
        LDEC.AdicionarInicio(2);
        LDEC.AdicionarInicio(1);
        LDEC.Listar();
        LDEC.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
//-----------------------------------------------------------------------------------------------//
        

        System.out.println("\n=============================================================================\n\n");
        
        System.out.println("            Buscando Valores!\n");
        LDEC.BuscarValor(3);
        LDEC.BuscarValor(8);
        LDEC.BuscarValor(0);
        System.out.println("\n=============================================================================\n\n");
        
//-----------------------------------------------------------------------------------------------//
        
        System.out.println("            Remover Valores No Inicio!");
        LDEC.RemoverInicio();
        LDEC.RemoverInicio();
        LDEC.Listar();
        LDEC.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
        
        
//-----------------------------------------------------------------------------------------------//
        
        
        
        
        
    }


}
