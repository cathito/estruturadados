package ListaDuplamenteEncadeada;

public class Teste {
    public static void main(String[] args) {
        LDE LDE = new LDE();

        System.out.println("            Adicionando Valores No Inicio!");
        LDE.AdicionarInicio(9);
        LDE.AdicionarInicio(8);
        LDE.AdicionarInicio(7);
        LDE.AdicionarInicio(6);
        LDE.AdicionarInicio(3);
        LDE.AdicionarInicio(2);
        LDE.AdicionarInicio(1);
        LDE.Listar();
        LDE.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
//-----------------------------------------------------------------------------------------------//
        

        System.out.println("            Adicionando Valores No Meio!");
        LDE.AdicionarMeio(4,4);
        LDE.AdicionarMeio(5,5);
        LDE.Listar();
        LDE.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
//-----------------------------------------------------------------------------------------------//
        
        System.out.println("            Adicionando Valores No Fim!");
        LDE.AdicionarFim(10);
        LDE.AdicionarFim(11);
        LDE.Listar();
        LDE.ListarContrario();

        System.out.println("\n=========================================================\n\n");
        
//-----------------------------------------------------------------------------------------------//
        
        System.out.println("            Remover Valores No Inicio!");
        LDE.RemoverInicio();
        LDE.Listar();
        LDE.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
        
        System.out.println("            Remover Valores No Meio!");
        LDE.RemoverMeio(3);
        LDE.Listar();
        LDE.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
        
        System.out.println("            Remover Valores No Fim!");
        LDE.RemoverFim();
        LDE.Listar();
        LDE.ListarContrario();
        System.out.println("\n-------------------------------------------------\n");
        
//-----------------------------------------------------------------------------------------------//
        System.out.println("\n=========================================================\n\n");
        
        System.out.println("            Buscando Valores!\n");
        LDE.BuscarValor(3);
        LDE.BuscarValor(8);
        LDE.BuscarValor(0);
        System.out.println("\n=========================================================\n\n");
        
//-----------------------------------------------------------------------------------------------//
        System.out.println("            Trocando Valor de Local!\n");
        LDE.trocaValorLocal(3, 1);
        
        LDE.Listar();
        System.out.println("\n\n");
        
        
//-----------------------------------------------------------------------------------------------//
        
        

    }


}
